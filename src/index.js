import Vue from 'vue'
// import Meta from 'vue-meta'
// import router from './router'
import store from './store'
import App from './App'
import 'fullpage.js/vendors/scrolloverflow'
import VueFullPage from 'vue-fullpage.js'

// Vue.use(Meta)
Vue.use(VueFullPage)

window.onload = () => {
  new Vue({
    // router,
    store,
    render: h => h(App)
  }).$mount('#app')
}
