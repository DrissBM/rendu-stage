import Vue from 'vue'
import Router from 'vue-router'
import vbclass from 'vue-body-class'

import Home from '../pages/Home'

const VueRouter = new Router({
  routes: [
    {
      path: '*',
      name: 'Home',
      component: Home,
      meta: { bodyClass: 'home' }
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

export default VueRouter

Vue.use(Router)
Vue.use(vbclass, VueRouter)
